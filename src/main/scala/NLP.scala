import collection.JavaConverters._
import org.apache.spark.sql.functions.udf
import org.json.JSONObject
import scala.math.pow

/*
 TODO:
 - Añadir eliminador de tildes - DONE
 - Buscar un stemmer/lematizer que merezca la pena
 - Polynomial features (dos pipelines diferentes)
 - Crear argumentos para definir input y output files
 - ¿Otros modelos?
 */

object Functions {
  def Param2JSON(element: org.apache.spark.ml.param.ParamMap):java.util.Map[String,String] = {
    return element.toSeq.map(x=>(x.param.parent+"-"+x.param.name, x.value.toString)).toMap.asJava
  }
  def Param2JSON(metrics: Array[Double],element: Array[org.apache.spark.ml.param.ParamMap]):JSONObject = {
    val paramList = element.map(datum=>datum.toSeq.map(x=>(x.param.parent+"-"+x.param.name, x.value.toString)).toMap)
    val joint = metrics.zip(paramList).map(pair=>(pair._2.updated("areaUnderROC",pair._1)).asJava)
    val jsondata = new JSONObject(Map("Resultados"->joint).asJava)
    return jsondata
  }
  def replaceTildes(string:String): String = {
    @annotation.tailrec
    def replaceRecursively(string: String, lookupTable:Array[Tuple2[String,String]]):String = {
      if (lookupTable.tail.isEmpty) return string.replace(lookupTable.head._1, lookupTable.head._2)
      val replacedString = string.replace(lookupTable.head._1, lookupTable.head._2)
      return replaceRecursively(replacedString, lookupTable.tail)
    }
    val diccionario = Array(("á","a"),("é","e"),("í","i"),("ó","o"),("ú","u"))
    replaceRecursively(string, diccionario)
  }
}

object MainClass {
  def main(args:Array[String]):Unit = {

    // Parsing de argumentos de la línea de comandos

    var datos_entrada = "/"
    var path_mejor_modelo = "/"
    var resultados_training = "/"

    if (args.indexOf("--entrada") == -1){
      println("Error al parsear. Se necesita definir --entrada [path_de_hdfs_hasta_los_datos_de_entrada]")
      System.exit(1)
    }
    else {
      datos_entrada = args(args.indexOf("--entrada")+1)
    }

    if (args.indexOf("--salida-mejor-modelo") == -1){
      println("Error al parsear. Se necesita definir --salida-mejor-modelo [path_de_hdfs]")
      System.exit(1)
    }
    else {
      path_mejor_modelo = args(args.indexOf("--salida-mejor-modelo")+1)
    }

    if (args.indexOf("--salida-resultados") == -1){
      println("Error al parsear. Se necesita definir --salida-resultados [path_de_hdfs]")
      System.exit(1)
    }
    else {
      resultados_training = args(args.indexOf("--salida-resultados")+1)
    }


    // Inicio del Programa

    val conf = (new org.apache.spark.SparkConf()
      .setAppName("NLP Amazon ES")
    )

    val sc = new org.apache.spark.SparkContext(conf)
    val sqlContext = new org.apache.spark.sql.hive.HiveContext(sc)

    val reviews_df = sqlContext.read.json(datos_entrada)
    val binarizer = udf(((x:Double) => if (x >= 3.5) "positivo" else "negativo"):(Double=>String))
    val lowercaser = udf(((x:String) => x.toLowerCase):(String=>String))
    val tildesreplacer = udf(Functions.replaceTildes:(String=>String))
    val binarizer_number = udf(((x:String) => if (x == "positivo") 1.0 else 0.0):(String=>Double))

    val reviews_df_good_bad = (reviews_df
      .withColumn("opinion", binarizer(reviews_df("estrellas")))
    )

    val reviews_df_lowercase = (reviews_df_good_bad
      .withColumn("comentario_lower", lowercaser(reviews_df_good_bad("comentario")))
    )

    val reviews_df_notildes = (reviews_df_lowercase
      .withColumn("comentario_cleaned", tildesreplacer(reviews_df_lowercase("comentario_lower")))
    )

    val reviews_df_binarized = (reviews_df_notildes
      .withColumn("sentimiento", binarizer_number(reviews_df_notildes("opinion")))
    )

    val input_dataset = (reviews_df_binarized
      .select("comentario", "comentario_cleaned", "estrellas", "sentimiento", "opinion")
      .distinct
      .repartition(16)
      .persist(org.apache.spark.storage.StorageLevel.MEMORY_AND_DISK_SER)
    )

    val tokenizer = (new org.apache.spark.ml.feature.RegexTokenizer()
      .setInputCol("comentario_cleaned")
      .setOutputCol("comentario_tk")
      .setPattern("""[\s.,:;()&@#$%"“”~‘’…«»►¨·•–¡!¿?'_/+*\-=]+""")
    )

    val ngrams = (new org.apache.spark.ml.feature.NGram()
      .setInputCol("comentario_tk")
      .setOutputCol("comentario_ngrams"))

    val tf = (new org.apache.spark.ml.feature.HashingTF()
      .setInputCol("comentario_ngrams")
      .setOutputCol("comentario_tf"))

    val idf = (new org.apache.spark.ml.feature.IDF()
      .setInputCol("comentario_tf")
      .setOutputCol("comentario_idf"))

    val lr = (new org.apache.spark.ml.classification.LogisticRegression()
      .setFeaturesCol("comentario_idf")
      .setLabelCol("sentimiento"))

    val pipe = (new org.apache.spark.ml.Pipeline()
      .setStages(Array(tokenizer, ngrams, tf, idf, lr)))


    val model_pool = (new org.apache.spark.ml.tuning.ParamGridBuilder()
      .addGrid(ngrams.n, Array(1,2,3))
      .addGrid(tf.numFeatures, Array(pow(2,18).toInt, pow(2,19).toInt, pow(2,20).toInt))
      .addGrid(lr.regParam, Array(0.0, 0.0001, 0.001, 0.01, 0.05, 0.1, 1.0))
      .addGrid(lr.elasticNetParam, Array(0.0, 0.25, 0.5, 0.75, 1.0))
      .addGrid(lr.maxIter, (40 to 360 by 90).toArray)
      .build
    )

    val evaluator = (new org.apache.spark.ml.evaluation.BinaryClassificationEvaluator()
      .setRawPredictionCol("rawPrediction")
      .setLabelCol("sentimiento")
      .setMetricName("areaUnderROC"))

    val validator = (new org.apache.spark.ml.tuning.CrossValidator()
      .setEstimator(pipe)
      .setEstimatorParamMaps(model_pool)
      .setEvaluator(evaluator)
      .setNumFolds(4)
    )

    val model = validator.fit(input_dataset)
    val mejor_modelo = model.bestModel.asInstanceOf[org.apache.spark.ml.PipelineModel]
    val mejor_modelo_JSON = new JSONObject(Map("Stages"->mejor_modelo
      .stages
      .map(stage=>stage.extractParamMap)
      .map(Functions.Param2JSON)).asJava)

    val estadisticas_training_JSON = Functions.Param2JSON(model.avgMetrics, model.getEstimatorParamMaps)
    

    /*val pw = new java.io.PrintWriter(new java.io.File("parametros.json"))
     pw.write(mejor_modelo_jsonString)
     pw.close*/

    (sc.parallelize(List(mejor_modelo_JSON.toString(2)))
      .coalesce(1)
      .saveAsTextFile(path_mejor_modelo)
    )
    (sc.parallelize(List(estadisticas_training_JSON.toString(2)))
      .coalesce(1)
      .saveAsTextFile(resultados_training)
    )
    sc.stop()
  }
}
