spark-submit --name "NLP Amazon Reviews ES" \
--class MainClass \
--master yarn \
--deploy-mode cluster \
--num-executors 1 \
--executor-cores 4 \
--executor-memory 24g \
--driver-memory 2g \
--jars /home/julio/Programas/Spark/latest/lib/datanucleus-api-jdo-3.2.6.jar,/home/julio/Programas/Spark/latest/lib/datanucleus-core-3.2.10.jar,/home/julio/Programas/Spark/latest/lib/datanucleus-rdbms-3.2.9.jar \
--files /home/julio/Programas/Spark/latest/conf/hive-site.xml,/home/julio/Programas/Spark/latest/conf/hbase-site.xml \
Codigo/sentiment-analysis-reviews-amazon-spain/target/scala-2.10/sentiment-analysis-amazon-es.jar \
--entrada hdfs:///user/julio/Amazon/tmpDataset \
--salida-mejor-modelo hdfs:///etl/staging/Amazon/Resultados/mejor_modelo.json \
--salida-resultados hdfs:///etl/staging/Amazon/Resultados/resultadosROC.json
