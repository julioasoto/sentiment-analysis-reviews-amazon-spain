name := "Sentiment Analysis Amazon ES"

version := "1.0"

scalaVersion := "2.10.4"

libraryDependencies += "org.apache.spark" %% "spark-core" % "1.5.2" % "provided"

libraryDependencies += "org.apache.spark" %% "spark-sql" % "1.5.2" % "provided"

libraryDependencies += "org.apache.spark" %% "spark-mllib" % "1.5.2" % "provided"

libraryDependencies += "org.apache.spark" %% "spark-hive" % "1.5.2" % "provided"

libraryDependencies += "org.json" % "json" % "20160212" % "provided"

// Java netlib, que implementa BLAS para mejor rendimiento de MLlib.
// Requiere tener instaladas libblas3, liblapack3 y libgfortran3.
// En Debian/Ubuntu, haríamos: sudo apt-get install libblas3 liblapack3 libgfortran3
libraryDependencies += "com.github.fommil.netlib" % "all" % "1.1.2"

// Nombre del assembly jar que se creará
jarName in assembly := "sentiment-analysis-amazon-es.jar"

// Para evitar que assembly incluya scala en sí 
assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false)

